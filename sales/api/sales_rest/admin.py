from django.contrib import admin
from.models import SalesRep, Sale, Customer, AutomobileVO

admin.site.register(Customer)
admin.site.register(SalesRep)
admin.site.register(Sale)
admin.site.register(AutomobileVO)
# Register your models here.
