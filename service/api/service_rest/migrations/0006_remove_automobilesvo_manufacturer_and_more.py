# Generated by Django 4.0.3 on 2022-08-03 02:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0005_alter_appointment_assigned_tech'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='automobilesvo',
            name='manufacturer',
        ),
        migrations.RemoveField(
            model_name='automobilesvo',
            name='name',
        ),
        migrations.RemoveField(
            model_name='automobilesvo',
            name='picture_url',
        ),
        migrations.AddField(
            model_name='automobilesvo',
            name='vin',
            field=models.CharField(default=0, max_length=100),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='ManufacturerVO',
        ),
    ]
