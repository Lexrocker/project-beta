from django.contrib import admin
from .models import Technician, Appointment, AutomobilesVO


admin.site.register(Technician)
admin.site.register(Appointment)
admin.site.register(AutomobilesVO)

# Register your models here.
